import torch  
from torchvision import transforms, models  
from PIL import Image  
  
# 确保在GPU或CPU上运行模型，取决于模型训练时使用的设备  
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")  
  
# 加载预训练的ResNet模型结构（不包括权重）  
model = models.resnet18(pretrained=False)  
  
# 更改最后一层的输出以适应您的分类任务（3个类别）  
num_ftrs = model.fc.in_features  
model.fc = torch.nn.Linear(num_ftrs, 3)  
  
# 加载之前保存的模型权重  
model.load_state_dict(torch.load('model_weights.pth', map_location=device))  
  
# 将模型设置为评估模式  
model.eval()  
model = model.to(device)  
  
# 图像预处理（应与训练时相同）  
transform = transforms.Compose([  
    transforms.Resize(256),  
    transforms.CenterCrop(224),  # 使用CenterCrop而不是RandomCrop进行预测  
    transforms.ToTensor(),  
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  
])  
  
# 加载并预处理图像  
def load_and_transform_image(image_path):  
    image = Image.open(image_path)  
    image = transform(image).unsqueeze(0).to(device)  
    return image  
  
# 预测函数  
def predict(image_path, model):  
    image = load_and_transform_image(image_path)  
    with torch.no_grad():  
        outputs = model(image)  
        _, predicted = torch.max(outputs, 1)  
    return predicted.item()  
  
# 示例：预测图像  
image_path = 'data/test/test_goat.jpg'  # 替换为您的图像路径  
predicted_class = predict(image_path, model)  
print(f'Predicted class: {predicted_class}')