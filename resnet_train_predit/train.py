import torch  
import torch.nn as nn  
import torch.optim as optim  
from torch.utils.data import DataLoader, random_split  
from torchvision import datasets, transforms, models  
  
# 数据增强  
transform = transforms.Compose([  
    transforms.Resize(256),  
    transforms.RandomCrop(224),  
    transforms.RandomHorizontalFlip(),  
    transforms.ToTensor(),  
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  
])  
  
# 假设你的图片存储在'data/train'目录下，并且已经按照类别组织好了  
dataset = datasets.ImageFolder(root='data/train', transform=transform)  
  
# 因为数据集很小，我们不划分验证集，只用于训练和测试  
train_size = int(0.8 * len(dataset))  
test_size = len(dataset) - train_size  
train_dataset, test_dataset = random_split(dataset, [train_size, test_size])  
  
train_loader = DataLoader(train_dataset, batch_size=4, shuffle=True)  
test_loader = DataLoader(test_dataset, batch_size=4, shuffle=False)  
  
# 使用GPU，如果可用  
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")  
  
# 加载预训练的ResNet模型  
model = models.resnet18(pretrained=True)  
  
# 冻结预训练模型的参数，只训练最后的全连接层  
for param in model.parameters():  
    param.requires_grad = False  
  
# 更改最后一层的输出以适应我们的分类任务（3个类别）  
num_ftrs = model.fc.in_features  
model.fc = nn.Linear(num_ftrs, 3)  
model = model.to(device)  
  
# 定义损失函数和优化器  
criterion = nn.CrossEntropyLoss()  
optimizer = optim.SGD(model.fc.parameters(), lr=0.001, momentum=0.9)  
  
# 训练过程  
num_epochs = 5  # 假设训练5个epoch  
  
for epoch in range(num_epochs):  
    model.train()  # 设置模型为训练模式  
    running_loss = 0.0  
    corrects = 0  
    total = 0  
    for images, labels in train_loader:  
        images, labels = images.to(device), labels.to(device)  
          
        # 梯度清零  
        optimizer.zero_grad()  
          
        # 前向传播  
        outputs = model(images)  
        _, predicted = torch.max(outputs, 1)  
          
        # 计算损失  
        loss = criterion(outputs, labels)  
          
        # 反向传播和优化  
        loss.backward()  
        optimizer.step()  
          
        # 统计数据  
        running_loss += loss.item() * images.size(0)  
        corrects += torch.sum(predicted == labels.data)  
        total += labels.size(0)  
      
    # 计算训练集上的准确率和平均损失  
    epoch_loss = running_loss / total  
    epoch_acc = 100. * corrects / total  
      
    # 验证步骤（在这个例子中，我们使用测试集作为验证集）  
    model.eval()  # 设置模型为评估模式  
    val_loss = 0.0  
    val_corrects = 0  
    val_total = 0  
    with torch.no_grad():  # 不计算梯度  
        for images, labels in test_loader:  
            images, labels = images.to(device), labels.to(device)  
            outputs = model(images)  
            loss = criterion(outputs, labels)  
            val_loss += loss.item() * images.size(0)  
            _, predicted = torch.max(outputs, 1)  
            val_corrects += torch.sum(predicted == labels.data)  
            val_total += labels.size(0)  
      
    # 计算验证集上的准确率和平均损失  
    val_epoch_loss = val_loss / val_total  
    val_epoch_acc = 100. * val_corrects / val_total  
      
    # 打印训练进度  
    print(f'Epoch {epoch+1}/{num_epochs}, Loss: {epoch_loss:.4f}, Train Acc: {epoch_acc:.2f}%, Val Loss: {val_epoch_loss:.4f}, Val Acc: {val_epoch_acc:.2f}%')  
  
# 评估模型（在测试集上）  
model.eval()  # 确保模型在评估模式下  
test_loss = 0.0  
test_corrects = 0  
test_total = 0  
with torch.no_grad():  
    for images, labels in test_loader:  
        images, labels = images.to(device), labels.to(device)  
        outputs = model(images)  
        loss = criterion(outputs, labels)  
        test_loss += loss.item() * images.size(0)  
        _, predicted = torch.max(outputs, 1)  
        test_corrects += torch.sum(predicted == labels.data)  
        test_total += labels.size(0)  
  
# 计算测试集上的准确率和平均损失  
test_loss = test_loss / test_total  
test_acc = 100. * test_corrects / test_total  
  
print(f'Test Loss: {test_loss:.4f}, Test Acc: {test_acc:.2f}%')

# 保存模型  
torch.save(model.state_dict(), 'model_weights.pth')